<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Dashboard extends Controller
{
    //load view
    function loadview(){
        $data = $this->data();
        return view('tiktok/dashboard', $data);
    }

    function data(){
        return ['name' => 'wildan'];
    }

}
